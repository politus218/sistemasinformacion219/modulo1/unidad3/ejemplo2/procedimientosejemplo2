﻿-- Tabla para poder actualizarla con los procedimientos.
CREATE TABLE datos (
  datos_id int(11) NOT NULL AUTO_INCREMENT,
  num1 int(11) NOT NULL,
  num2 int(11) NOT NULL,
  suma varchar(255) DEFAULT NULL,
  resta varchar(255) DEFAULT NULL,
  rango varchar(5) DEFAULT NULL,
  texto1 varchar(25) DEFAULT NULL,
  texto2 varchar(25) DEFAULT NULL,
  junto varchar(255) DEFAULT NULL,
  longitud varchar(255) DEFAULT NULL,
  tipo int(11) NOT NULL,
  num int(11) NOT NULL,
  PRIMARY KEY (datos_id)
                    )
  ENGINE = INNODB,
  CHARACTER SET latin1,
  COLLATE latin1_swedish_ci,
  ROW_FORMAT = COMPACT,
  TRANSACTIONAL = 0;
  ALTER TABLE datos
    ADD INDEX num2_index(num2);
  ALTER TABLE datos
    ADD INDEX num_index(num);
  ALTER TABLE datos
    ADD INDEX tipo_index(tipo);
