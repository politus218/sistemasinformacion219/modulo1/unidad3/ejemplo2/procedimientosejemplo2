﻿-- Ejercicio 1. Realizar un procedimiento almacenado que reciba un texto y un caracter.
-- Debe indicarte si ese carácter está en el texto. Se debe hacer con: Locate, Position.

DELIMITER //
  CREATE OR REPLACE PROCEDURE caracterentexto(texto varchar(100), caracter varchar(1)) 
    BEGIN
      DECLARE resul varchar(50) DEFAULT "El caracter no está en el texto";
      IF (LOCATE(caracter,texto)) THEN
      SET resul = "El caracter está en el texto";
      END IF;
      SELECT resul;
    END //
  DELIMITER ;

CALL caracterentexto("Me llamo Txondo, Gorka Txondo", "x");
SELECT @resul;

-- Ejercicio 2. Realizar un procedimiento almacenado que reciba un texto y un caracter.
-- Te debe indicar todo el texto que haya antes de la primera vez que aparece ese caracter.

DELIMITER //
  CREATE OR REPLACE PROCEDURE textoantesdecaracter(texto varchar(100), caracter varchar(1))
    BEGIN
      DECLARE resul varchar(50) DEFAULT NULL;
      SET resul=SUBSTRING_INDEX(texto,caracter,1);
      SELECT resul;
    END //
  DELIMITER ;

CALL textoantesdecaracter("Me llamo Txondo, Gorka Txondo","x");
SELECT @resul;

-- Ejercicio 3. Realizar un procedimiento almacenado que reciba tres números y dos
-- argumentos de tipo salida donde devuelva el nº más grande y el nº más pequeño de
-- los tres números pasados.

DELIMITER //
    CREATE OR REPLACE PROCEDURE mayorymenordetres(n1 int, n2 int, n3 int, OUT mayor int, OUT menor int)
      BEGIN
        SET mayor=GREATEST(n1,n2,n3);
        SET menor=LEAST(n1,n2,n3);
        END //
    DELIMITER;

CALL mayorymenordetres (2,5,7, @mayor, @menor);
SELECT @mayor;
SELECT @menor;
      
-- Ejercicio 4. Realizar un procedimiento almacenado que muestre cuantos números1
-- y números2 son mayores que 50.

  DELIMITER //
    CREATE OR REPLACE PROCEDURE mayorescincuenta(num1 int, num2 int)
      BEGIN
        DECLARE resul int;
        DECLARE n1 int;
        DECLARE n2 int; 
        SELECT COUNT(*) INTO n1 FROM datos WHERE num1>50;
        SELECT n2=(SELECT COUNT(*) FROM datos WHERE num2)>50;
        SET resul=n1+n2;
      END //
    DELIMITER ;

CALL mayorescincuenta(42,52);

-- Ejercicio 5. Realizar un procedimiento almacenado que calcule la suma y la resta
-- de número1 y número2 de la tabla datos.

DELIMITER //
  CREATE OR REPLACE PROCEDURE sumaentabladatos()
    BEGIN
      
      UPDATE datos         
      SET suma = num1 + num2,
        resta = num1 - num2;      
    END //
  DELIMITER ;

  CALL sumaentabladatos();

-- Ejercicio 6. Realizar un procedimiento almacenado que primero ponga todos los 
-- valores de suma y resta a NULL y después calcule la suma solamente si el número1
-- es mayor que el número 2 y calcule la resta de número2 - número1 si el número 2
-- es mayor o número1 - número2 si es mayor el número1.

DELIMITER //
  CREATE OR REPLACE PROCEDURE sumayoresta()
    BEGIN
      UPDATE datos
      SET suma=NULL, 
       resta=NULL;
      UPDATE datos
        SET suma= num1+num2
         WHERE num1>num2;
      UPDATE datos
        SET resta= num1-num2
        where num1>num2;
      UPDATE datos
        SET resta=num2-num1
        WHERE num1<=num2;
    END //
  DELIMITER ;                  

  CALL sumayoresta();

-- Ejercicio 7. Realizar un procedimiento almacenado que coloque en el campo junto
-- el texto1, texto2.

  DELIMITER //
    CREATE OR REPLACE PROCEDURE junto()
      BEGIN
        UPDATE datos
       -- DECLARE junto varchar(50);
        SET junto=CONCAT(texto1, texto2);
      END //
    DELIMITER ;

  CALL junto();
  

-- Ejercicio 8.
-- Realizar un procedimiento almacenado que coloque en el campo junto el valor NULL.
-- Después debe colocar en el campo junto el texto1-texto2, si el rango es A. Y si 
-- es rango B debe colocar texto1+texto2. Si el rango es distinto debe colocar texto1
-- nada más.

  DELIMITER //
  CREATE OR REPLACE PROCEDURE ejemplo8() 
  BEGIN
    UPDATE datos
      SET junto=NULL;
    UPDATE datos
      SET texto1=TRIM(texto1), texto2=TRIM(texto2);
    UPDATE datos
      SET junto=IF(rango='A',CONCAT(texto1,"-",texto2),IF(rango='B',CONCAT(texto1,"+",texto2), texto1));
         
  END //
  DELIMITER;

  CALL ejemplo8();
